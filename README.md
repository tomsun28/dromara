<p align="center">
<img src="https://dromara.org/img/logo/dromara.jpg" height="220"/>
</p>

<div align="center">

为往圣继绝学，一个人或许能走的更快，但一群人会走的更远。

</div>

## 我们的愿景

让每一位开源爱好者，体会到开源的快乐。

## 社区口号

技术栈全面开源共建、保持社区中立、和谐快乐做开源 。

## 官网

**[https://dromara.org](https://dromara.org)** 是 **Dromara** 开源社区官方网站。


## 社区项目

#### 目前已加入 **Dromara** 社区的顶级项目包括：

| 正式项目                                                                                                                | 关注量                                                                                                                                                                                                                                                           | 项目介绍                                                |     官网                     |
| ---------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| -----------------------------------------------------  | --------------------------- |
| [![Hmily](https://img.shields.io/badge/hmily-blue)](https://gitee.com/dromara/hmily)                                   | [![star](https://gitee.com/dromara/hmily/badge/star.svg?theme=gvp)](https://gitee.com/dromara/hmily/stargazers) [![star](https://img.shields.io/github/stars/dromara/hmily.svg)](https://github.com/dromara/hmily/stargazers)                                   | 高性能一站式分布式事务解决方案。                            |  [dromara.org](https://dromara.org)        |
| [![Raincat](https://img.shields.io/badge/raincat-blue)](https://gitee.com/dromara/Raincat)                             | [![star](https://gitee.com/dromara/Raincat/badge/star.svg?theme=gvp)](https://gitee.com/dromara/Raincat/stargazers) [![star](https://img.shields.io/github/stars/dromara/Raincat.svg)](https://github.com/dromara/Raincat/stargazers)                           | 强一致性分布式事务解决方案。                               |  [dromara.org](https://dromara.org)        |
| [![Myth](https://img.shields.io/badge/myth-blue)](https://gitee.com/dromara/myth)                                      | [![star](https://gitee.com/dromara/myth/badge/star.svg?theme=gvp)](https://gitee.com/dromara/myth/stargazers) [![star](https://img.shields.io/github/stars/dromara/myth.svg)](https://github.com/dromara/myth/stargazers)                                       | 可靠消息分布式事务解决方案。                               |  [dromara.org](https://dromara.org)        |
| [![LiteFlow](https://img.shields.io/badge/liteFlow-blue)](https://gitee.com/dromara/liteFlow)                          | [![star](https://gitee.com/dromara/liteFlow/badge/star.svg?theme=gvp)](https://gitee.com/dromara/liteFlow/stargazers) [![star](https://img.shields.io/github/stars/dromara/liteFlow.svg)](https://github.com/dromara/liteFlow/stargazers)                       | 一个轻量，快速的组件式流程引擎框架。                        | [liteflow.yomahub.com](https://liteflow.yomahub.com) |
| [![TLog](https://img.shields.io/badge/TLog-blue)](https://gitee.com/dromara/TLog)                                      | [![star](https://gitee.com/dromara/TLog/badge/star.svg?theme=gvp)](https://gitee.com/dromara/TLog/stargazers) [![star](https://img.shields.io/github/stars/dromara/TLog.svg)](https://github.com/dromara/TLog/stargazers)                                       | 轻量级的分布式日志标记追踪神器。                           | [tlog.yomahub.com](https://tlog.yomahub.com)        |
| [![Forest](https://img.shields.io/badge/forest-blue)](https://gitee.com/dromara/forest)                                | [![star](https://gitee.com/dromara/forest/badge/star.svg?theme=dark)](https://gitee.com/dromara/forest/stargazers) [![star](https://img.shields.io/github/stars/dromara/forest.svg)](https://github.com/dromara/forest/stargazers)                              | 高层的、极简的轻量级HTTP调用API框架。                      | [forest.dtflyx.com](https://forest.dtflyx.com)        |
| [![hutool](https://img.shields.io/badge/hutool-blue)](https://gitee.com/dromara/hutool)                                | [![star](https://gitee.com/dromara/hutool/badge/star.svg?theme=gvp)](https://gitee.com/dromara/hutool/stargazers) [![star](https://img.shields.io/github/stars/dromara/hutool.svg)](https://github.com/dromara/hutool/stargazers)                               | 一个使Java保持甜美的工具类库。                            | [hutool.cn](https://hutool.cn)               |
| [![MaxKey](https://img.shields.io/badge/MaxKey-blue)](https://gitee.com/dromara/MaxKey)                                | [![star](https://gitee.com/dromara/MaxKey/badge/star.svg?theme=gvp)](https://gitee.com/dromara/MaxKey/stargazers) [![star](https://img.shields.io/github/stars/dromara/MaxKey.svg)](https://github.com/dromara/MaxKey/stargazers)                               | 业界领先的企业级开源IAM身份管理和身份认证产品。              | [maxkey.top](https://www.maxkey.top)          |
| [![cubic](https://img.shields.io/badge/cubic-blue)](https://gitee.com/dromara/cubic)                                   | [![star](https://gitee.com/dromara/cubic/badge/star.svg?theme=gvp)](https://gitee.com/dromara/cubic/stargazers) [![star](https://img.shields.io/github/stars/dromara/cubic.svg)](https://github.com/dromara/cubic/stargazers)                                   | 无侵入分布式监控，致力于应用级监控，帮助开发人员快速定位问题。  | [cubic.jiagoujishu.com](https://cubic.jiagoujishu.com)   |
| [![Jpom](https://img.shields.io/badge/Jpom-blue)](https://gitee.com/dromara/Jpom)                                      | [![star](https://gitee.com/dromara/Jpom/badge/star.svg?theme=gvp)](https://gitee.com/dromara/Jpom/stargazers) [![star](https://img.shields.io/github/stars/dromara/Jpom.svg)](https://github.com/dromara/Jpom/stargazers)                                       | 一款简而轻的低侵入式在线构建、自动部署、日常运维、项目监控软件。| [jpom.io](https://jpom.io)                 |
| [![sa-token](https://img.shields.io/badge/saToken-blue)](https://gitee.com/dromara/sa-token)                           | [![star](https://gitee.com/dromara/sa-token/badge/star.svg?theme=gvp)](https://gitee.com/dromara/sa-token/stargazers) [![star](https://img.shields.io/github/stars/dromara/sa-token.svg)](https://github.com/dromara/sa-token/stargazers)                        | 史上功能最全的Java权限认证框架！。                        | [sa-token.dev33.cn](https://sa-token.dev33.cn)        |
| [![sureness](https://img.shields.io/badge/sureness-blue)](https://gitee.com/dromara/sureness)                          | [![star](https://gitee.com/dromara/sureness/badge/star.svg?theme=dark)](https://gitee.com/dromara/sureness/stargazers) [![star](https://img.shields.io/github/stars/dromara/sureness.svg)](https://github.com/dromara/sureness/stargazers)                      | 面向REST API的高性能认证鉴权框架。                        | [usthe.com/sureness](https://usthe.com/sureness)      |
| [![koalas-rpc](https://img.shields.io/badge/koalasRpc-blue)](https://gitee.com/dromara/koalas-rpc)                     | [![star](https://gitee.com/dromara/koalas-rpc/badge/star.svg?theme=gvp)](https://gitee.com/dromara/koalas-rpc/stargazers) [![star](https://img.shields.io/github/stars/dromara/koalas-rpc.svg)](https://github.com/dromara/koalas-rpc/stargazers)                | 企业生产级百亿日PV高可用可拓展的RPC框架。                  |  [dromara.org](https://dromara.org)             |


### 目前已加入 **Dromara** 社区的孵化项目包括：

| 孵化项目                                                                                                                | 关注量                                                                                                                                                                                                                                                                       | 项目介绍                                                                                                  |        官网                    |
| ---------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- | ----------------------------- |
| [image-combiner](https://gitee.com/dromara/image-combiner)                                                             | [![star](https://gitee.com/dromara/image-combiner/badge/star.svg?theme=dark)](https://gitee.com/dromara/image-combiner/stargazers)                                                                                                                                         |专门用于图片合成的工具。                                                                                      | [image-combiner](https://dromara.org)  | 
| [northstar](https://gitee.com/dromara/northstar)                                                                       | [![star](https://gitee.com/dromara/northstar/badge/star.svg?theme=dark)](https://gitee.com/dromara/northstar/stargazers) [![star](https://img.shields.io/github/stars/dromara/northstar.svg)](https://github.com/dromara/northstar/stargazers)                             |自动化交易平台。                                                                                             | [dromara.org](https://dromara.org)                         |
| [fast-request](https://gitee.com/dromara/fast-request)                                                                 | [![star](https://gitee.com/dromara/fast-request/badge/star.svg?theme=dark)](https://gitee.com/dromara/fast-request/stargazers) [![star](https://img.shields.io/github/stars/dromara/fast-request.svg)](https://github.com/dromara/fast-request/stargazers)                 |IDEA httpClient插件。                                                                                       | [plugins.sheng90.wang](https://plugins.sheng90.wang/fast-request) |
| [open-giteye-api](https://gitee.com/dromara/open-giteye-api)                                                           | [![star](https://gitee.com/dromara/open-giteye-api/badge/star.svg?theme=dark)](https://gitee.com/dromara/open-giteye-api/stargazers) [![star](https://img.shields.io/github/stars/dromara/open-giteye-api.svg)](https://github.com/dromara/open-giteye-api/stargazers)     |专为开源作者设计的数据图表服务工具类站点，提供了包括Star趋势图、贡献者列表、Gitee指数等数据图表服务。                     | [giteye.net](https://giteye.net)    |
| [hertzbeat](https://gitee.com/dromara/hertzbeat)                                                                       | [![star](https://gitee.com/dromara/hertzbeat/badge/star.svg?theme=gvp)](https://gitee.com/dromara/hertzbeat/stargazers) [![star](https://img.shields.io/github/stars/dromara/hertzbeat.svg)](https://github.com/dromara/hertzbeat/stargazers)                              |易用友好的高性能监控告警系统。                                                                                   | [hertzbeat.com](https://hertzbeat.com) |
| [easy-es](https://gitee.com/dromara/easy-es)                                                                           | [![star](https://gitee.com/dromara/easy-es/badge/star.svg?theme=dark)](https://gitee.com/dromara/easy-es/stargazers) [![star](https://img.shields.io/github/stars/dromara/easy-es.svg)](https://github.com/dromara/easy-es/stargazers)                                     |一款简化ElasticSearch搜索引擎操作的开源框架,简化CRUD操作,可以更好的帮助开发者减轻开发负担。                             | [easy-es.cn](https://easy-es.cn) |
| [dynamic-tp](https://gitee.com/dromara/dynamic-tp)                                                                     | [![star](https://gitee.com/dromara/dynamic-tp/badge/star.svg?theme=dark)](https://gitee.com/dromara/dynamic-tp/stargazers) [![star](https://img.shields.io/github/stars/dromara/dynamic-tp.svg)](https://github.com/dromara/dynamic-tp/stargazers)                         |轻量级，基于配置中心实现对运行中线程池参数的动态修改，以及实时监控线程池                                                | [dynamictp.cn](https://dynamictp.cn) |
| [mendmix](https://gitee.com/dromara/mendmix)                                                                           | [![star](https://gitee.com/dromara/mendmix/badge/star.svg?theme=gvp)](https://gitee.com/dromara/mendmix/stargazers) [![star](https://img.shields.io/github/stars/dromara/mendmix.svg)](https://github.com/dromara/mendmix/stargazers)                                      |java企业级应用开发套件，定位是一站式分布式开发架构开源解决方案及云原生架构技术底座                                       | [jeesuite.com](https://www.jeesuite.com) |
| [gobrs-async ](https://gitee.com/dromara/gobrs-async)                                                                  | [![star](https://gitee.com/dromara/gobrs-async/badge/star.svg?theme=dark)](https://gitee.com/dromara/gobrs-async/stargazers) [![star](https://img.shields.io/github/stars/dromara/gobrs-async.svg)](https://github.com/dromara/gobrs-async/stargazers)                     |一款功能强大、配置灵活、带有全链路异常回调、内存优化、异常状态管理于一身的高性能异步编排框架                                | [async.sizegang.cn](https://async.sizegang.cn) |
| [x-easypdf](https://gitee.com/dromara/x-easypdf )                                                                      | [![star](https://gitee.com/dromara/x-easypdf/badge/star.svg?theme=dark)](https://gitee.com/dromara/gx-easypdf/stargazers)                                                                                                                                                  |一个用搭积木的方式构建pdf的框架（基于pdfbox）                                                                      | [x-easypdf](https://xsxgit.gitee.io/x-easypdf) |


**[点击查看全部项目](https://gitee.com/organizations/dromara/projects)**

## 加入组织

 **Dromara** 社区欢迎所有热爱开源的小伙伴加入，可以点击组织界面 【[申请加入组织](https://gitee.com/dromara)】链接进行申请，也可以发送电子邮件至 [xiaoyu@dromara.org](mailto:xiaoyu@dromara.org) 联系我们。

## 组织责任

- 组织不得从事违法或损人利己的事情
- 负责社区新旧捐赠项目评审工作
- 负责新旧社区成员管理工作
- 负责社区下所有孵化项目推广，宣传和项目版本更新日志维护
- 负责统筹和执行社区组织的活动

## 行为准则

- 捐赠项目准则：所有捐赠给 **Dromara** 社区的项目均保留原有项目的所有权利及商业化行为，**Dromara** 社区除协助推广、宣传、运营不做任何干涉，但捐赠项目不得从事违法行为或诋毁 **Dromara** 社区名声。

- 社区成员准则：不得从事违法或损人利己的事情。

- 社区项目：不得从事违法或损人利己的事情


## 项目捐赠

Dromara开源社区组织对捐赠项目有以下最基本条件要求：

- **项目必须是原创且不能是 Fork 版本。**
- 项目必须是完整的应用解决方案。
- 项目必须有良好的代码注释和不断完善的使用文档。
- 项目必须在 Gitee 平台托管且 Stars 关注量大于 100。
- 项目至少在近 2 个月内有实际有效的代码提交记录。
- **项目必须得到 dromara 社区组织评委会的认可。**
- 优先考虑 Gitee 推荐项目或已获得 GVP 的项目。（GVP 项目需联系 Gitee 官方人员取消 GVP 后才能转移到组织，转移成功后恢复 GVP）
- 优选考虑 容器工具、微服务框架和工具、分布式事务、分布式中间件、大数据处理、人工智能、IoT 物联网、开发/测试/运维相关工具链等领域项目。

**[点击查看项目捐赠](https://gitee.com/dromara/dromara/blob/master/project-donate.md)**


## 赞助/支持

**Dromara** 社区能够持续运营和提供更加优质的服务离不开大家的支持。如果想成为 **Dromara** 社区赞助商或支持者，请考虑支持：

 <img src="https://dromara.org/img/donation/donation.png" height="240">

**Dromara** 社区承诺将收到的所有赞助支持资金完全公开化，且后续资金用途仅限于 **Dromara** 社区运营支出。

**[点击查看全部赞助商](https://dromara.gitee.io/donate.html)**

## 联系我们

- 捐赠项目/加入组织：[xiaoyu@dromara.org](mailto:xiaoyu@dromara.org)
